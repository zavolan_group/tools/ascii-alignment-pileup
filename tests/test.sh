#!/usr/bin/env bash

# Tear down test environment
cleanup () {
    rc=$?
    rm "test.test-mir.min.1.pileup.tab"
    cd $user_dir
    echo "Exit status: $rc"
}
trap cleanup EXIT

# Set up test environment
set -eo pipefail  # ensures that script exits at first command that exits with non-zero status
set -u  # ensures that script exits when unset variables are used
set -x  # facilitates debugging by printing out executed commands
user_dir=$PWD
script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd $script_dir

../ascii_alignment_pileup.R \
  --verbose \
  --reference="test_files/test.fa.gz" \
  --annotations="test_files/test.gff" \
  --output-directory="$PWD" \
  "test_files/test.bed" \
  "test_files/test.bam"

# Check md5 sum of output file
md5sum --check "expected_output.md5"
